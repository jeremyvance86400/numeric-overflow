'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8000;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main 
app.get('/', (req, res) => {
    try {
        approval(parseInt(req.query.amount)) ? 
            res.status(400).end(req.query.amount + ' requires approval.') : 
            res.status(200).end(req.query.amount + ' does not require approval.');
    } catch(ex) {
        console.log(ex);
        res.status(400).end('Invalid input');
    }
});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
var approval = (value) => {
    var threashold = 1000;
    var surcharge = 10;
    var validated = validate(value);
  
    if (!validated) {
        return false;
    }
    var amount = Int32Array.from([validated],x=>parseInt(x+surcharge));
    if (amount[0] >= threashold) {
        return true;
    };
    return false;
};

var validate = (value) => {
    if (typeof(value) !== 'number' || value == 'NaN') {
        throw new TypeError();
    }

    if ((Math.abs(value) >= Math.pow(2, 32)/2 - 10) || value <= 0) {
        throw new RangeError();
    }

    return value;
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, approval, validate };
